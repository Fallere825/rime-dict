#include "config.h"

int str_remove(string &str) { //删除字符串中的换行符
    int move = 0;
    int length1=str.length();
    for (int i=0; i<str.length(); ++i) {
        str[i-move]=str[i];
        if (str[i]=='\n') move++;
    }
    str.erase(str.length()-move);
    return 0;
}

int readconfig2(ifstream &infile, kv config[]) {
    string buf;
    int i=0;
    while (getline(infile, buf, ';')) { //getline要用单引号
        //拆分一个分号为一个键值对
        istringstream buf2(buf);
        getline(buf2, config[i].key, ':');
        getline(buf2, config[i].value, ':');
        //去除\n
        str_remove(config[i].key);
        str_remove(config[i].value);
        //计数器+1
        i++;
    }
    if (i=0) return 1;
    return 0; //返回值暂时无用，将来可以表示是否操作成功
}

int writeconfig2(string filename, kv config[]) {
    ofstream output;
    output.open(filename, ios::out);
    output.close();
    output.open(filename, ios::out|ios::app);
    int i=0;
    while (config[i].key!="") {
        output<<config[i].key<<":"<<config[i].value<<";\n";
        i++;
    }
    output.close();
    if (i=0) return 1;
    return 0;
}
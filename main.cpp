/* tips:
stoi()          string->int
to_string()     int->string
*/
#include <curl/curl.h>
#include <cstring>
#include <config.h>
#include <iostream>
using namespace std;

size_t curlWriteFunction(void* ptr, size_t size, size_t nmemb, FILE* stream) {
    return fwrite(ptr, size, nmemb, stream);
}

int download(string filename, string url) {
    CURL* curl = curl_easy_init();
    if (curl) {
        FILE* fp = nullptr;
        errno_t ret=fopen_s(&fp, filename.c_str(), "w");    // 打开文件，准备写入
        if (ret==3) { //该文件不存在，则创建
            ofstream newfile(filename);
            newfile.close();
        }
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteFunction);
        curl_easy_perform(curl);
        fclose(fp);                        // 关闭文件
        curl_easy_cleanup(curl);
        cout<<"successfully download "<<filename<<endl;
        return 0;
    }
    return 2;
}

int main() {
    curl_global_init(CURL_GLOBAL_DEFAULT);
    string urlhead="https://raw.kkgithub.com/iDvel/rime-ice/main/";

    //1.读取文件配置
    ifstream downloadfiles;
    kv config[MAX];
    downloadfiles.open("downloadfiles.config", ios::in);
    readconfig2(downloadfiles, config);

    // 2.读取并且下载需要更新的文件列表
    int i=0;
    while (config[i].key!="") {
        time_t timep;
        time(&timep);
        if (timep-stoi((config[i].value))>3600) {  //如果两次运行的时间接近就不重复下载了
            download("C:/Users/zyh/AppData/Roaming/Rime/"+config[i].key, urlhead+config[i].key);
            config[i].value=to_string(timep);
        }
        else cout<<"ingnoring file "<<config[i].key<<endl;
        i++;
    }
    //3.回写配置文件
    writeconfig2("downloadfiles.config", config);

    curl_global_cleanup();
    return 0;
}